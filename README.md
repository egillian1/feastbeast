# FeastBeast
Meal planner and organizer

## Development
* Start off by running `bundle install` to get all packages.
* Run `rails server` to start web server for development purposes.

### Tips & Tricks
Use the command `rails console --sandbox` to start a console interface with
the back end (that rolls backs all changes made on exit) to experiment with.
